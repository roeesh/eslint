import log from "@ajar/marker";
import { saySomething } from "./myModule.js";

const response = saySomething("hello");
log.magenta(response);

((x: number) => log.blue(x, "check new rule - arrow spacing"))(2);

//feature2 changes
log.cyan("this is feature 2");

//merges main and feature--add-linting
